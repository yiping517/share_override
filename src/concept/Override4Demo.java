package concept;

public class Override4Demo {

	public static void main(String[] args) {

	}
}

class Coo{
	void show() {}
	double say() {return 0.0;}
	Coo test() {return null;}
	Doo re() {return null;}
}

class Doo extends Coo{
//	int show() {return 1;} //編譯錯誤
//	int say() {return 1;} 
	Doo test() {return null;} //父類大，子類小
//	Coo re() {return null;}
}