package concept;

public class Override6Demo {

	public static void main(String[] args) {

//		Ioo i = new Ioo(2); //編譯錯誤
		//證明 : constructor不是繼承的。而是super()
	}

}

class Hoo{
	Hoo(){}
	Hoo(int a){}
}

class Ioo extends Hoo{
	Ioo(){}
}