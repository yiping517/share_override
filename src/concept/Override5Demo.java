package concept;
//override VS overload
public class Override5Demo {

	//驗證
	public static void main(String[] args) {
		//overload看引用、override看對象
		//目標 : 調用test()
		//1.需要對象
		Goo goo = new Goo();
		//2.需要參數
		Eoo o = new Foo();
		goo.test(o);
		//test() overload。overload看引用，而o的引用是Eoo型，所以調的是test(Eoo o)
		//進到該方法後，show() override了。而override看對象。對象是Foo，所以調用的是Foo的show()
	}

}

//override : 
class Eoo{
	void show() {
		System.out.println("父類");
	}
}

class Foo extends Eoo{
	void show() {
		System.out.println("子類");
	}
}

//overload : 
class Goo{
	void test(Eoo o) {
		System.out.println("父型參數");
		o.show();
	}
	void test(Foo o) {
		System.out.println("子型參數");
		o.show();
	}
}