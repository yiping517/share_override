package concept;

public class Override3Demo {
	public static void main(String[] args) {
		Aoo a = new Aoo();
		a.show();
		
		Boo b = new Boo();
		b.show();
		
		//override方法被調用時，看對象的類型。即，new的是誰，調誰。
		Aoo oo = new Boo();
		oo.show();
	}
}

class Aoo{
	void show() {
		System.out.println("父類");
	}
}

class Boo extends Aoo{
	void show() {
		System.out.println("子類");
	}
}